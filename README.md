# Tobi-P

This repo contains the software required to setup and run the Tobi-P.

<img src="https://cdn.hackaday.io/images/2173771630238812384.jpg"  width="250">

## Preparation

A SD card of 4GB might work, but 8GB is recommended.

### OS Installation

- Download and install the latest [Raspberry Pi Imager](https://www.raspberrypi.org/software/).
- Select the latest Raspberry Pi OS Lite (2021-05-07 as the time of writing) under "Operating System" -> Raspberry Pi OS (other).
- Choose your SD card in the "Storage" tab.
- Press Ctrl-Shift-X to open the advanced options.
- Set the hostname to tobi.
- Enable SSH and choose a password.
- Click "Save", then "Write", and wait for the download + copy to finish.

Alternative (in Linux), download the latest image and flash it directly to the SD card:

```
sudo dd if=2021-05-07-raspios-buster-armhf.img of=/dev/sdX bs=4M conv=fsync status=progress
```

(also create an empty "ssh" file on the /boot partition for headless operation)


### OS Setup

Insert the SD card in the Pi, plug the Ethernet cable, power it up, ssh into it, and configure a few things:

```
sudo raspi-config
```

```
1 System Options -> S5 Boot / Auto Login -> B2 Console Autologin
3 Interface Options -> P1 Camera -> Yes
4 Performance Options -> P1 Overclock -> High
                      -> P2 GPU Memory -> 16 MB
5 Localisation Options -> L2 Timezone -> Select yours
                       -> L4 WLAN Country -> Select yours
6 Advanced Options -> A1 Expand Filesystem -> Ok
```

Finish and reboot to apply the changes.

_**Note:**_  
*Using the Edimax EW-7811UN as a wireless adapter, I had problems later on with Hostapd and had to run:*
```
sudo rm /etc/modprobe.d/blacklist-rtl8*
```

Optionally, also get the latest system updates:
```
sudo apt update
sudo apt -y full-upgrade
sudo reboot
```

### Swap

512MB of RAM is not enough for the full installation below.

In this case increasing the swap (at least temporarily) really helps:
```
sudo dphys-swapfile swapoff
sed 's/^CONF_SWAPSIZE=.*/CONF_SWAPSIZE=1024/' /etc/dphys-swapfile
sudo dphys-swapfile setup
sudo dphys-swapfile swapon
```

## Installation

Clone this repo in the correct folder.
```
sudo apt install git
git clone https://gitlab.com/jdelbe/tobi-p.git ~/tobi_catkin_ws/src/tobi-p
```

Then simply start the installation script, which should take care of the rest.
```
cd ~/tobi_catkin_ws/src/tobi-p/tobi_bringup/scripts
./install.bash
```

 :warning:  **Warning:**  

The ROS installation from sources can take several _**HOURS**_ !! (At least on the first generation of Pi).

_**Note:**_  
*There is no need for the sudo password in the scripts, as the 'pi' user has everything authorized by default in the sudoers file.*


## Usage

After a successful installation and reboot, the LAN cable can be disconnected.

## Troubleshooting

- Check the installation logs to identify problems (_~/tobi_catkin_ws/src/tobi-p/tobi_bringup/logs_)
- For networking issues: https://wiki.ros.org/ROS/NetworkSetup
I occasionally had to add a line in /etc/hosts on the RPi when the pc hostname was not recognized
- Try to run a faulty script individually (_~/tobi_catkin_ws/src/tobi-p/tobi_bringup/scripts_)
- Changing the "-j" option in ros_install.bash can help in some cases (e.g: on the Pi3, setting -j1 can prevent a lockup during _catkin_make_isolated_)
- Check the Raspicam is correctly plugged:
```
raspistill -o test.jpg
```
- Check the servo is correctly plugged:
```
import RPi.GPIO as GPIO
import time

GPIO.setmode(GPIO.BOARD)
GPIO.setup(12, GPIO.OUT)
p = GPIO.PWM(12, 50)
p.start(7.5)

try:
    while True:
        p.ChangeDutyCycle(7.5)  # turn towards 90 degree
        time.sleep(1) # sleep 1 second
        p.ChangeDutyCycle(2.5)  # turn towards 0 degree
        time.sleep(1) # sleep 1 second
except KeyboardInterrupt:
    p.stop()
    GPIO.cleanup()
```
- Check the driver/motors are correctly connected
```
# Init driver & mode
sudo pi-blaster ; echo "27=1" > /dev/pi-blaster
# Left motor: forward, 1/4 speed
echo "23=0" > /dev/pi-blaster ; echo "22=0.25" > /dev/pi-blaster
# Right motor: forward, 1/4 speed
echo "25=0" > /dev/pi-blaster ; echo "24=0.25" > /dev/pi-blaster
# Stop motors
echo "22=0" > /dev/pi-blaster ; echo "24=0" > /dev/pi-blaster
```
