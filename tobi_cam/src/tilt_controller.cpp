#include <fstream>
#include <ros/ros.h>
#include "tobi_cam/CamTiltRequest.h"


/*****************************************************************************
 * Public Constants
 ****************************************************************************/
constexpr uint8_t GPIO_PIN            = 18;  // TODO: as param ?
constexpr int8_t MIN_ANGLE            = -90;
constexpr int8_t MAX_ANGLE            = 90;

/*****************************************************************************
 * Private Constants
 ****************************************************************************/
static constexpr float MIN_RAW_VALUE         = 0.045;
static constexpr float MAX_RAW_VALUE         = 0.254;

// Note: As of C++14 the next following lines can be moved to the body of getRawValue:
static constexpr float RATIO = (MAX_RAW_VALUE - MIN_RAW_VALUE) / (MAX_ANGLE - MIN_ANGLE);
static constexpr float OFFSET = MIN_RAW_VALUE - MIN_ANGLE * RATIO;

/*****************************************************************************
 * Private variables
 ****************************************************************************/
static std::ofstream outputFile;

/*****************************************************************************
 * Private Functions
 ****************************************************************************/
/**
 * @brief   Convert a requested angle value (degre) to a valid pi-blaster value
 * @param   desiredAngle: The requested angle
 * @return  The raw value to be sent to pi-blaster
 */
static constexpr float getRawValue(int8_t desiredAngle)
{
    return desiredAngle * RATIO + OFFSET;
}

/**
 * @brief   Setup function, to be called once before the node starts
 * @param   setupAngle: The initial desired tilt angle
 * @return  true on success, false otherwise
 */
static bool setupNode(int setupAngle)
{
    if (setupAngle < MIN_ANGLE || setupAngle > MAX_ANGLE) {
        ROS_ERROR("Invalid value, range is [%d:%d]", MIN_ANGLE, MAX_ANGLE);
        return false;
    }

    ROS_DEBUG("Starting pi-blaster");

    system("sudo pi-blaster");

    outputFile.open("/dev/pi-blaster");
    if ( !outputFile) {
        ROS_ERROR("Failure on opening pi-blaster's file");
        return false;
    }

    ROS_INFO("Resetting camera's tilt to %d deg ...", setupAngle);

    outputFile << std::setprecision(3);
    outputFile << static_cast<int>(GPIO_PIN) << "=" << getRawValue(static_cast<int8_t>(setupAngle));
    outputFile << std::endl;

    return true;
}

/*****************************************************************************
 * Public Functions
 ****************************************************************************/
/**
 * @brief   Callback for tilt requests
 * @param   msg: The expected angle
 */
void tiltRequestCallback(const tobi_cam::CamTiltRequest::ConstPtr& msg)
{
    if (msg->angle < MIN_ANGLE || msg->angle > MAX_ANGLE) {
        ROS_WARN("Invalid value, range is [%d:%d]", MIN_ANGLE, MAX_ANGLE);
        return;
    }

    outputFile << static_cast<int>(GPIO_PIN) << "=" << getRawValue(msg->angle);
    outputFile << std::endl;

    //ROS_VERBOSE("Tilt angle requested: %d", msg->angle); 
}

int main(int argc, char **argv)
{
    ros::init(argc, argv, "tilt_controller");

    // Retrieve the potential private parameters
    ros::NodeHandle nhPrivate("~");
    int startupAngle;
    nhPrivate.param<int>("startup_angle", startupAngle, 0);

    if ( !setupNode(startupAngle)) {
        ROS_ERROR("Problem with pi-blaster; node shutting down");
        return 1;
    }

    // Actual start of the node
    ros::NodeHandle nh;
    ros::Subscriber sub = nh.subscribe("camera/tilt", 50, tiltRequestCallback);

    ros::spin();
    // outputFile.close(); // Not needed, as std::fstream is a proper RAII object
    return 0;
}
