#include <fstream>
#include <ros/ros.h>
#include <geometry_msgs/Twist.h>


/*****************************************************************************
 * Private Constants
 ****************************************************************************/
static constexpr float MAX_MOTOR_SPEED                 = 1.0;
static constexpr bool STARTUP_LEFT_MOTOR_DIR           = 0;
static constexpr bool STARTUP_RIGHT_MOTOR_DIR          = 0;
static constexpr float DEFAULT_LEFT_LINEAR_RATIO       = 2.0;
static constexpr float DEFAULT_LEFT_ANGULAR_RATIO      = 4.0;
static constexpr float DEFAULT_LEFT_OFFSET             = 0;
static constexpr float DEFAULT_RIGHT_LINEAR_RATIO      = 2.0;
static constexpr float DEFAULT_RIGHT_ANGULAR_RATIO     = 4.0;
static constexpr float DEFAULT_RIGHT_OFFSET            = 0;
static constexpr int DEFAULT_VEL_CMD_TIMEOUT_MS        = 1000;
/*
 * Current wiring of the Pololu DRV8835 to the Pi B+ pins
 * TODO: as params ?
 */
static constexpr uint8_t DRIVER_MODE_PIN     = 27; // 1 for Phase/Enable mode, the easiest
static constexpr uint8_t LEFT_PWM_PIN        = 22; // from 0 to 1, precision of about 0.01 
static constexpr uint8_t LEFT_DIRECTION_PIN  = 23; // 0 for Forward, 1 for Reverse
static constexpr uint8_t RIGHT_PWM_PIN       = 24;
static constexpr uint8_t RIGHT_DIRECTION_PIN = 25;

/*****************************************************************************
 * Private variables
 ****************************************************************************/
static std::ofstream outputFile;
static float leftLinRatio       = DEFAULT_LEFT_LINEAR_RATIO;
static float leftAngRatio       = DEFAULT_LEFT_ANGULAR_RATIO;
static float leftOffset         = DEFAULT_LEFT_OFFSET;
static float rightLinRatio      = DEFAULT_RIGHT_LINEAR_RATIO;
static float rightAngRatio      = DEFAULT_RIGHT_ANGULAR_RATIO;
static float rightOffset        = DEFAULT_RIGHT_OFFSET;
static int velCmdTimeout        = DEFAULT_VEL_CMD_TIMEOUT_MS;
static ros::Timer velCmdWatchdog;

/*****************************************************************************
 * Private Functions
 ****************************************************************************/
/**
 * @brief   Convert a requested velocity to a valid pi-blaster value for the 
            left motor
 * @param   vel: The requested velocity
 * @return  The raw speed value to be sent to pi-blaster
 */
static float getLeftMotorSpeed(const geometry_msgs::Twist::ConstPtr& vel)
{
    return vel->linear.x / leftLinRatio - vel->angular.z / leftAngRatio + leftOffset;
}

/**
 * @brief   Convert a requested velocity to a valid pi-blaster value for the 
            right motor
 * @param   vel: The requested velocity
 * @return  The raw speed value to be sent to pi-blaster
 */
static float getRightMotorSpeed(const geometry_msgs::Twist::ConstPtr& vel)
{
    return vel->linear.x / rightLinRatio + vel->angular.z / rightAngRatio + rightOffset;
}

/**
 * @brief   Set motors speed to 0, stopping them
 * @return  true on success, false otherwise
 */
static bool stopMotors()
{
    outputFile << static_cast<int>(LEFT_PWM_PIN)  << "=" << 0 << std::endl;
    outputFile << static_cast<int>(RIGHT_PWM_PIN) << "=" << 0 << std::endl;

    return true;
}

/**
 * @brief   Setup function, to be called once before the node starts
 * @return  true on success, false otherwise
 */
static bool setupNode(bool startPiBlaster)
{
    if (startPiBlaster) {

        ROS_WARN("Starting pi-blaster");

        system("sudo pi-blaster");
    }
    else {
        // pi-blaster MUST somehow be initialized, but it could be done from another node
        // Let's wait a little bit to ensure this is the case, also with a launch file for instance
        ROS_INFO("sleeping...");
        ros::Duration(10).sleep();
    }

    outputFile.open("/dev/pi-blaster");
    if ( !outputFile) {
        ROS_ERROR("Failure on opening pi-blaster's file");
        return false;
    }

    ROS_INFO("Setting up driver mode ...");

    outputFile << std::setprecision(2);
    outputFile << static_cast<int>(DRIVER_MODE_PIN) << "=" << 1 << std::endl;

    ROS_INFO("Resetting motors ...");

    outputFile << static_cast<int>(LEFT_DIRECTION_PIN)  << "=" << STARTUP_LEFT_MOTOR_DIR  << std::endl;    
    outputFile << static_cast<int>(RIGHT_DIRECTION_PIN) << "=" << STARTUP_RIGHT_MOTOR_DIR << std::endl;

    return stopMotors();
}

/*****************************************************************************
 * Public Functions
 ****************************************************************************/
/**
 * @brief   Callback for the cmd_vel watchdog
 * @param   event: Time infos, useful for debugging
 * @note    No race condition risk, as only one callback is executed at a time with ros::spin() !
 */
void velCmdWatchdogCallback(const ros::TimerEvent& event)
{
    stopMotors();

    velCmdWatchdog.stop();
}

/**
 * @brief   Callback for cmd_vel requests
 * @param   msg: The expected angle
 */
void velCmdCallback(const geometry_msgs::Twist::ConstPtr& msg)
{
    velCmdWatchdog.stop();
    velCmdWatchdog.setPeriod(ros::Duration(velCmdTimeout / 1000), true);

    static bool currentLeftMotorDir  = STARTUP_LEFT_MOTOR_DIR;
    static bool currentRightMotorDir = STARTUP_RIGHT_MOTOR_DIR;

    float leftMotorSpeed  = getLeftMotorSpeed(msg);
    float rightMotorSpeed = getRightMotorSpeed(msg);

    bool leftMotorDir  = (leftMotorSpeed  < 0) ? 1 : 0;
    bool rightMotorDir = (rightMotorSpeed < 0) ? 1 : 0;

    if (currentLeftMotorDir != leftMotorDir) {
        currentLeftMotorDir = leftMotorDir;
        outputFile << static_cast<int>(LEFT_DIRECTION_PIN)  << "=" << leftMotorDir  << std::endl;    
    }
    if (currentRightMotorDir != rightMotorDir) {
        currentRightMotorDir = rightMotorDir;
        outputFile << static_cast<int>(RIGHT_DIRECTION_PIN) << "=" << rightMotorDir << std::endl;
    }

    leftMotorSpeed =  fabs(leftMotorSpeed);
    rightMotorSpeed = fabs(rightMotorSpeed);

    if (leftMotorSpeed > MAX_MOTOR_SPEED || rightMotorSpeed > MAX_MOTOR_SPEED) {
        ROS_WARN("Invalid value, max raw speed is [%.2f]", MAX_MOTOR_SPEED);
        return;
    }
    
    outputFile << static_cast<int>(LEFT_PWM_PIN)  << "=" << leftMotorSpeed  << std::endl;
    outputFile << static_cast<int>(RIGHT_PWM_PIN) << "=" << rightMotorSpeed << std::endl;

    velCmdWatchdog.start();
}

int main(int argc, char **argv)
{
    ros::init(argc, argv, "base_controller");

    // Retrieve the potential private parameters
    ros::NodeHandle nhPrivate("~");
    nhPrivate.param<float>("left_speed_linear_ratio",   leftLinRatio,    DEFAULT_LEFT_LINEAR_RATIO);
    nhPrivate.param<float>("left_speed_angular_ratio",  leftAngRatio,    DEFAULT_LEFT_ANGULAR_RATIO);
    nhPrivate.param<float>("left_speed_offset",         leftOffset,      DEFAULT_LEFT_OFFSET);
    nhPrivate.param<float>("right_speed_linear_ratio",  rightLinRatio,   DEFAULT_RIGHT_LINEAR_RATIO);
    nhPrivate.param<float>("right_speed_angular_ratio", rightAngRatio,   DEFAULT_RIGHT_ANGULAR_RATIO);
    nhPrivate.param<float>("right_speed_offset",        rightOffset,     DEFAULT_RIGHT_OFFSET);
    nhPrivate.param<int>("cmd_vel_timeout_ms",          velCmdTimeout,   DEFAULT_VEL_CMD_TIMEOUT_MS);
    bool startPiBlaster;
    nhPrivate.param<bool>("start_pi_blaster",           startPiBlaster,  true);

    if (velCmdTimeout < 0) {
        ROS_ERROR("Timeout value cannot be negative; node shutting down");
        return 1;
    }

    if ( !setupNode(startPiBlaster)) {
        ROS_ERROR("Problem with pi-blaster; node shutting down");
        return 1;
    }

    // Actual start of the node
    ros::NodeHandle nh;
    ros::Subscriber sub = nh.subscribe("cmd_vel", 50, velCmdCallback);
    velCmdWatchdog      = nh.createTimer(ros::Duration(velCmdTimeout / 1000), velCmdWatchdogCallback);
    velCmdWatchdog.stop();

    ros::spin();
    // outputFile.close(); // Not needed, as std::fstream is a proper RAII object
    return 0;
}
