#!/bin/bash

WORKSPACE_DIR=~/Workspace/Tobi-P/catkin_ws
CONFIG_DIR=${WORKSPACE_DIR}/src/tobi-p/tobi_bringup/config

export ROS_IP=$(grep 'address=' ${CONFIG_DIR}/workstation.machine | cut -f 4 -d '"')
export ROS_MASTER_URI=http://$(grep 'address=' ${CONFIG_DIR}/tobi.machine | cut -f 4 -d '"'):11311

source /opt/ros/melodic/setup.bash
source ${WORKSPACE_DIR}/devel/setup.bash

exec "$@"
