#!/bin/bash
##############################################################################
###   Generic cleanup on exit
##############################################################################
trap cleanup EXIT
function cleanup() {
    local pids=$(jobs -pr)
    if [[ -n ${pids} ]]; then
        kill ${pids}
    fi
}

##############################################################################
###   Strict error handling
##############################################################################
set -Eeo pipefail ## exit on command or pipeline failure
trap 'error_report >&2' ERR ## stdout redirected to stderr for the function
function error_report() {
    local errCode="${?}" ## will also exit with this code
    local lastCmd="${BASH_COMMAND}"
    echo ""
    echo "ERROR   @ line $(caller)"
    echo "=>   \"${lastCmd}\" failed [code ${errCode}]"
}

##############################################################################
###   Ctrl+C handling
##############################################################################
trap user_interrupt INT
function user_interrupt() {
    echo ""
    echo "EXIT - Cancelled by user (Ctrl+C)"
    exit 130 ## exit status should be 128 + signal number
}

##############################################################################
###   Main
##############################################################################
## Remove everything for retries
sudo rm -Rf /etc/apt/sources.list.d/ros-latest.list /opt/ros/ ~/.ros/ /etc/ros/ ~/ros_catkin_ws/

sudo sh -c 'echo "deb http://packages.ros.org/ros/ubuntu $(lsb_release -sc) main" > /etc/apt/sources.list.d/ros-latest.list'
curl -s https://raw.githubusercontent.com/ros/rosdistro/master/ros.asc | sudo apt-key add -
sudo apt update

sudo apt-get -y install python3-rosdep python3-rosinstall-generator python3-vcstool build-essential git

sudo rosdep init
rosdep update

mkdir -p ~/ros_catkin_ws
cd ~/ros_catkin_ws

rosinstall_generator ros_comm compressed_image_transport camera_info_manager dynamic_reconfigure diagnostic_updater --rosdistro noetic --deps --wet-only --exclude roslisp --tar > noetic-tobi-p_wet.rosinstall
mkdir ./src
## The next command can use -j
vcs import --input noetic-tobi-p_wet.rosinstall ./src

rosdep install --from-paths ./src --ignore-src --rosdistro noetic -y -r

## The next command can use -j
sudo ./src/catkin/bin/catkin_make_isolated -q --install --install-space /opt/ros/noetic -DCMAKE_BUILD_TYPE=Release -DPYTHON_EXECUTABLE=/usr/bin/python3

source /opt/ros/noetic/setup.bash ## Not persistent yet
