#!/bin/bash
#############################################################################
###   Generic cleanup on exit
#############################################################################
trap cleanup EXIT
function cleanup() {
    local pids=$(jobs -pr)
    if [[ -n ${pids} ]]; then
        kill ${pids}
    fi
    date -u -d "0 ${SECONDS} sec" +"Exit [Total %Hh%Mm%Ss]"
}

##############################################################################
###   Strict error handling
##############################################################################
set -Eeuo pipefail ## exit on command or pipeline failure or unset variable
trap 'error_report >&2' ERR ## stdout redirected to stderr for the function
function error_report() {
    local errCode="${?}" ## will also exit with this code
    local lastCmd="${BASH_COMMAND}"
    echo ""
    echo "ERROR   @ line $(caller)"
    echo "=>   \"${lastCmd}\" failed [code ${errCode}]"
    echo "=>   See also logs in ~/tobi_catkin_ws/src/tobi-p/tobi_bringup/logs"
}

##############################################################################
###   Ctrl+C handling
##############################################################################
trap user_interrupt INT
function user_interrupt() {
    echo ""
    echo "INT - Cancelled by user (Ctrl+C)"
    exit 130 ## exit status should be 128 + signal number
}

##############################################################################
###   Spinner handling, as a background process to show some activity
##############################################################################
function spinner() {
    echo -n " "
    while true; do
        for X in '-' '\' '|' '/'; do
            echo -en "\b${X}"
            sleep 0.5
        done
    done
}
function start_spinner() {
    if [[ ! -v spinnerPid ]]; then
        spinner &
        spinnerPid=$!
        startDate=${SECONDS}
    fi
}
function stop_spinner() {
    if [[ -v spinnerPid ]]; then
        kill -PIPE ${spinnerPid} ## remove traces from kill
        unset spinnerPid
        echo -en "\bdone "
        date -u -d "0 ${SECONDS} sec - ${startDate} sec" +"[%Hh%Mm%Ss]"
    fi
}

##############################################################################
###   Main
##############################################################################
mkdir -p ../logs # TODO handle better with variables + paths in all subscripts

echo "-----------------------------------------------------------------------"
echo "                ***  Tobi-P software installation  ***"
echo "-----------------------------------------------------------------------"
echo -n "[1/5] Installing ROS from sources (several HOURS !!) ....... "
start_spinner
bash ./install_ros.bash > ../logs/out-install_ros.log 2> ../logs/err-install_ros.log
stop_spinner

echo -n "[2/5] Installing pi-blaster ................................ "
start_spinner
bash ./install_pi-blaster.bash > ../logs/out-install_pi-blaster.log 2> ../logs/err-install_pi-blaster.log
stop_spinner

echo -n "[3/5] Installing raspicam_node ............................. "
start_spinner
bash ./install_raspicam_node.bash > ../logs/out-install_raspicam_node.log 2> ../logs/err-install_raspicam_node.log
stop_spinner

echo -n "[4/5] Setting up our ROS workspace ......................... "
start_spinner
bash ./install_tobi_workspace.bash > ../logs/out-install_tobi_workspace.log 2> ../logs/err-install_tobi_workspace.log
stop_spinner

echo -n "[5/5] Setting up the Access Point .......................... "
start_spinner
bash ./install_wireless_ap.bash > ../logs/out-install_wireless_ap.log 2> ../logs/err-install_wireless_ap.log
stop_spinner

echo "Please reboot and ensure the wireless access point is running correctly."
