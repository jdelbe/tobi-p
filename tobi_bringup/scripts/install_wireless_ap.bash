#!/bin/bash
##############################################################################
###   Generic cleanup on exit
##############################################################################
trap cleanup EXIT
function cleanup() {
    local pids=$(jobs -pr)
    if [[ -n ${pids} ]]; then
        kill ${pids}
    fi
}

##############################################################################
###   Strict error handling
##############################################################################
set -Eeuo pipefail ## exit on command or pipeline failure or unset variable
trap 'error_report >&2' ERR ## stdout redirected to stderr for the function
function error_report() {
    local errCode="${?}" ## will also exit with this code
    local lastCmd="${BASH_COMMAND}"
    echo ""
    echo "ERROR   @ line $(caller)"
    echo "=>   \"${lastCmd}\" failed [code ${errCode}]"
}

##############################################################################
###   Ctrl+C handling
##############################################################################
trap user_interrupt INT
function user_interrupt() {
    echo ""
    echo "EXIT - Cancelled by user (Ctrl+C)"
    exit 130 ## exit status should be 128 + signal number
}

##############################################################################
###   Main
##############################################################################

## https://www.raspberrypi.org/documentation/configuration/wireless/access-point.md

scriptTime=$(date +'%Y-%m-%d_%H%M%S')

sudo apt-get -y install dnsmasq hostapd
sudo systemctl stop dnsmasq
sudo systemctl stop hostapd

cd ~/tobi_catkin_ws/src/tobi-p/tobi_bringup/config

## Configuring a static IP (dhcpcd)
sudo cp /etc/dhcpcd.conf /etc/dhcpcd.conf.bak_${scriptTime}
sudo cp dhcpcd.conf /etc/dhcpcd.conf
sudo service dhcpcd restart

## Configuring the DHCP server (dnsmasq)
sudo cp /etc/dnsmasq.conf /etc/dnsmasq.conf.bak_${scriptTime}
sudo cp dnsmasq.conf /etc/dnsmasq.conf
sudo systemctl restart dnsmasq

## Configuring the access point host software (hostapd)
if [[ -f "/etc/hostapd/hostapd.conf" ]]; then
    sudo cp /etc/hostapd/hostapd.conf /etc/hostapd/hostapd.conf.bak_${scriptTime}
fi
sudo cp hostapd.conf /etc/hostapd/hostapd.conf

sudo cp /etc/default/hostapd /etc/default/hostapd.bak_${scriptTime}
sudo sh -c "echo DAEMON_CONF=\"/etc/hostapd/hostapd.conf\" > /etc/default/hostapd"

sudo systemctl unmask hostapd
sudo systemctl enable hostapd
sudo systemctl start hostapd

## Add routing and masquerade
sudo cp /etc/sysctl.conf /etc/sysctl.conf.bak_${scriptTime}
sudo cp sysctl.conf /etc/sysctl.conf

sudo iptables -t nat -A  POSTROUTING -o eth0 -j MASQUERADE
sudo sh -c "iptables-save > /etc/iptables.ipv4.nat"

sudo cp /etc/rc.local /etc/rc.local.bak_${scriptTime}
sudo cp rc.local /etc/rc.local
