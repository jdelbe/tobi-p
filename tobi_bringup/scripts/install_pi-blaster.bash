#!/bin/bash
##############################################################################
###   Generic cleanup on exit
##############################################################################
trap cleanup EXIT
function cleanup() {
    local pids=$(jobs -pr)
    if [[ -n ${pids} ]]; then
        kill ${pids}
    fi
}

##############################################################################
###   Strict error handling
##############################################################################
set -Eeuo pipefail ## exit on command or pipeline failure or unset variable
trap 'error_report >&2' ERR ## stdout redirected to stderr for the function
function error_report() {
    local errCode="${?}" ## will also exit with this code
    local lastCmd="${BASH_COMMAND}"
    echo ""
    echo "ERROR   @ line $(caller)"
    echo "=>   \"${lastCmd}\" failed [code ${errCode}]"
}

##############################################################################
###   Ctrl+C handling
##############################################################################
trap user_interrupt INT
function user_interrupt() {
    echo ""
    echo "EXIT - Cancelled by user (Ctrl+C)"
    exit 130 ## exit status should be 128 + signal number
}

##############################################################################
###   Main
##############################################################################
sudo apt-get -y install autoconf

cd ~

## Remove for retries
sudo rm -Rf pi-blaster

git clone https://github.com/sarfata/pi-blaster.git

cd pi-blaster
./autogen.sh
./configure
make
sudo make install

sudo sh -c 'echo "DAEMON_OPTS=\"--gpio 18,22,24\"" > /etc/default/pi-blaster'
sudo ./pi-blaster --gpio 18,22,24
