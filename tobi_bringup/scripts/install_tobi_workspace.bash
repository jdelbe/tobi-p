#!/bin/bash
##############################################################################
###   Generic cleanup on exit
##############################################################################
trap '' EXIT

##############################################################################
###   Strict error handling
##############################################################################
set -Eeo pipefail ## exit on command or pipeline failure
trap 'error_report >&2' ERR ## stdout redirected to stderr for the function
function error_report() {
    local errCode="${?}" ## will also exit with this code
    local lastCmd="${BASH_COMMAND}"
    echo ""
    echo "ERROR   @ line $(caller)"
    echo "=>   \"${lastCmd}\" failed [code ${errCode}]"
}

##############################################################################
###   Ctrl+C handling
##############################################################################
trap user_interrupt INT
function user_interrupt() {
    echo ""
    echo "EXIT - Cancelled by user (Ctrl+C)"
    exit 130 ## exit status should be 128 + signal number
}

##############################################################################
###   Main
##############################################################################
scriptTime=$(date +'%Y-%m-%d_%H%M%S')

## Not persistent yet
source /opt/ros/noetic/setup.bash

cd ~/tobi_catkin_ws/
catkin_make

## Permanently source the environment loader from .bashrc
if ! grep -q "source ~/tobi_catkin_ws/src/tobi-p/tobi_bringup/scripts/env_tobi.bash" ~/.bashrc; then
    cp ~/.bashrc ~/.bashrc.bak_${scriptTime}
    echo "source ~/tobi_catkin_ws/src/tobi-p/tobi_bringup/scripts/env_tobi.bash" >> ~/.bashrc
fi
