#!/bin/bash
##############################################################################
###   Generic cleanup on exit
##############################################################################
trap cleanup EXIT
function cleanup() {
    local pids=$(jobs -pr)
    if [[ -n ${pids} ]]; then
        kill ${pids}
    fi
}

##############################################################################
###   Strict error handling
##############################################################################
set -Eeo pipefail ## exit on command or pipeline failure
trap 'error_report >&2' ERR ## stdout redirected to stderr for the function
function error_report() {
    local errCode="${?}" ## will also exit with this code
    local lastCmd="${BASH_COMMAND}"
    echo ""
    echo "ERROR   @ line $(caller)"
    echo "=>   \"${lastCmd}\" failed [code ${errCode}]"
}

##############################################################################
###   Ctrl+C handling
##############################################################################
trap user_interrupt INT
function user_interrupt() {
    echo ""
    echo "EXIT - Cancelled by user (Ctrl+C)"
    exit 130 ## exit status should be 128 + signal number
}

##############################################################################
###   Main
##############################################################################
source /opt/ros/noetic/setup.bash ## Not persistent yet

cd ~/tobi_catkin_ws/src

## Remove for retries
sudo rm -Rf raspicam_node

git clone https://github.com/UbiquityRobotics/raspicam_node.git
cd raspicam_node
git checkout -b tobi-p 0.5.0
cd ..

sudo sh -c 'echo "yaml https://raw.githubusercontent.com/UbiquityRobotics/rosdep/master/raspberry-pi.yaml" > /etc/ros/rosdep/sources.list.d/30-ubiquity.list'
rosdep update

cd ~/tobi_catkin_ws
rosdep install --from-paths src/raspicam_node --ignore-src --rosdistro noetic -y
